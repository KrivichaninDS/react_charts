export const graphData = [
  { id: 0, name: 'Page A', uv: 4000, pv: 2400, amt: 2400 },
  { id: 1, name: 'Page B', uv: 3000, pv: 1398, amt: 2210 },
  { id: 2, name: 'Page C', uv: 2000, pv: 9800, amt: 2290 },
  { id: 3, name: 'Page D', uv: 2780, pv: 3908, amt: 2000 },
  { id: 4, name: 'Page E', uv: 1890, pv: 4800, amt: 2181 },
  { id: 5, name: 'Page F', uv: 2390, pv: 3800, amt: 2500 },
  { id: 6, name: 'Page G', uv: 3490, pv: 4300, amt: 2100 },
  { id: 7, name: 'Page M', uv: 3800, pv: 2500, amt: 1800 },
];

const makeRequest = (method, url, params) => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = () => {
      if (xhr.status >= 200 && xhr.status <= 300) {
        // reject(new Error('Server error'));
        resolve(xhr.response);
      } else {
        reject({
          status: xhr.status,
          statusText: xhr.statusText,
        });
      }
    };

    xhr.onerror = () => {
      reject({
        status: xhr.status,
        statusText: xhr.statusText,
      });
    };

    xhr.send(params);
  });
};


export const fetchRoute = (from, to) => {
  return makeRequest('POST', '/route/getRoute', JSON.stringify(
    {
      Coordinates: {
        from,
        to,
      },
    }));
};
