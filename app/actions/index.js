import * as api from '../src/api';
import * as consts from '../consts';

export const fetchChartData = () => (dispatch) => {
  return dispatch({
    type: consts.FETCH_CHART_DATA_SUCCESS,
    data: api.graphData,
  });
};

export function addChartData(data) {
  return {
    type: consts.ADD_CHART_DATA,
    data,
  };
}
