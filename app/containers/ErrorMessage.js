import React, { PropTypes } from 'react';

const ErrorMessage = ({ message, onRetry }) => (
  <div>
    <p>Error message: {message}</p>
    <button onClick={onRetry}>Retry</button>
  </div>
);

ErrorMessage.propTypes = {
  message: PropTypes.string.isRequired,
  onRetry: PropTypes.func.isRequired,
};

export default ErrorMessage;
