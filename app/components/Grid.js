import React, { Component, PropTypes } from 'react';

class Grid extends Component {
  componentDidMount() {
    this.renderGrid();
  }

  componentDidUpdate() {
    this.renderGrid();
  }

  renderGrid() {
    d3.select(this.node).call(this.props.grid);
  }
  render() {
    const translate = `translate(0,${this.props.h})`;
    return (
      <g
        className="y-grid"
        transform={this.props.gridType === 'x' ? translate : ''}
        ref={(node) => { this.node = node; }}
      />
    );
  }
}

Grid.propTypes = {
  h: PropTypes.number,
  grid: PropTypes.func,
  gridType: PropTypes.oneOf(['x', 'y']),
};

export default Grid;
