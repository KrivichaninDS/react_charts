import React, { Component, PropTypes } from 'react';

class Axis extends Component {

  componentDidMount() {
    this.renderAxis();
  }

  componentDidUpdate() {
    this.renderAxis();
  }

  renderAxis() {
    d3.select(this.node).call(this.props.axis);
  }

  render() {
    const translate = `translate(0,${this.props.h})`;

    return (
      <g
        transform={this.props.axisType === 'x' ? translate : ''}
        className="axis"
        ref={(node) => { this.node = node; }}
      />
    );
  }
}
Axis.propTypes = {
  h: PropTypes.number,
  axis: PropTypes.func,
  axisType: PropTypes.oneOf(['x', 'y']),

};

export default Axis;
