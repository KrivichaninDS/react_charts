import { combineReducers } from 'redux';
import * as consts from '../consts';

const chart = () => {
  const getChartData = (state = [], action) => {
    switch (action.type) {
      case consts.FETCH_CHART_DATA_SUCCESS:
        return action.data;
      case consts.ADD_CHART_DATA:
        return [
          ...state,
          {
            id: action.data.id,
            name: action.data.name,
            uv: action.data.uv,
            pv: action.data.pv,
            amt: action.data.amt,
          },
        ];
      default:
        return state;
    }
  };


  return combineReducers({
    getChartData,

  });
};

export default chart;

export const getChartData = state => state.getChartData;
