import { combineReducers } from 'redux';

import chart, * as fromChart from './Chart';

const Chart = chart();

export default combineReducers({
  Chart,
});

export const getChartData = (state) => {
  return fromChart.getChartData(state.Chart);
};
