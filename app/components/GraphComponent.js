import React, { Component, PropTypes } from 'react';
import { LineChart, Line, XAxis, YAxis,
  CartesianGrid, Tooltip, Legend } from 'recharts';
import Header from 'grommet/components/Header';
import Box from 'grommet/components/Box';
import { browserHistory } from 'react-router';

class GraphComponent extends Component {
  componentDidMount() {
    this.goToTestChart = () => {
      browserHistory.push('/test');
    };
  }

  render() {
    const { graphData, loadChartData } = this.props;
    return (
      <div>
        <Box>
          <Header>
            <h3>Example chart</h3>
          </Header>
          <LineChart
            width={600} height={300} data={graphData}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
          >
            <XAxis dataKey="name" />
            <YAxis />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Legend />
            <Line
              type="monotone"
              dataKey="pv"
              stroke="#8884d8" activeDot={{ r: 8 }}
            />
            <Line
              type="monotone"
              dataKey="uv"
              stroke="#82ca9d"
              activeDot={{ r: 8 }}
            />
          </LineChart>
        </Box>
        <button onClick={loadChartData}>Add data</button>
        <p>
          <button onClick={this.goToTestChart}>Go to test chart</button>
        </p>
      </div>
    );
  }
}

GraphComponent.propTypes = {
  loadChartData: PropTypes.func.isRequired,
  graphData: PropTypes.array.isRequired,
};

export default GraphComponent;
