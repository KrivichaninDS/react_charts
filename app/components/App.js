import React from 'react';
import GraphContainer from '../containers/GraphContainer';
import '../styles/styles.css';
import '../../node_modules/grommet/grommet.min.css';

const App = () => (
  <div>
    <GraphContainer />
  </div>
);

export default App;
