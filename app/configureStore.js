import { createStore, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

import appReducers from './reducers';

const configureStore = () => {
  const middlewares = [
    thunk,
    createLogger(),
  ];

  return createStore(
    appReducers,
    applyMiddleware(...middlewares),
  );
};

export default configureStore;
