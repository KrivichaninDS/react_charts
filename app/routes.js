import React from 'react';
import { Route } from 'react-router';

import App from './components/App';
import TestChartContainer from './containers/TestChartContainer';

const routes = (
  <div>
    <Route path="/" component={App} />
    <Route path="/test" component={TestChartContainer} />
  </div>
);

export default routes;
