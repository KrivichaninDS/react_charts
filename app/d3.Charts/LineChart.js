export const x = (args) => {
  return d3.time.scale()
    .domain([args.now - (args.limit - 2), args.now - args.duration])
    .rangeRound([0, args.w]);
};

export const y = (args) => {
  return d3.scale.linear()
    .domain([0, 100])
    .range([args.h, 0]);
};

export const yAxis = (args) => {
  return d3.svg.axis()
    .scale(args.y)
    .orient('left')
    .ticks(5);
};

export const yGrid = (args) => {
  return d3.svg.axis()
    .scale(args.y)
    .orient('left')
    .ticks(5)
    .tickSize(-(args.w), 0, 0)
    .tickFormat('');
};

