const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config');

const express = require('express');
const bodyParser = require('body-parser');
const routers = require('./routers');

const nconf = require('./config');

const app = express();

const compiler = webpack(webpackConfig);
app.use(webpackDevMiddleware(compiler, nconf.get('webpackDevMiddleware')));
app.use(webpackHotMiddleware(compiler));
app.use(bodyParser.json());

app.use('/', routers);


const server = app.listen(nconf.get('port'), (err) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log('server listening on port: %s', nconf.get('port'));
});

const io = require('socket.io')(server);

io.on('connection', (socket) => {
  socket.on('getData', () => {
    const send = setInterval(() => {
      socket.emit('setData', {
        chunk: Math.random() * 100,
      });
    }, 100);

    setTimeout(() => {
      clearInterval(send);
    }, 4000);
  });

  socket.on('getChartData', () => {
    socket.emit('setChartData', {
      id: 7, name: 'Page M', uv: 3800, pv: 2500, amt: 1800,
    });
  });
});
