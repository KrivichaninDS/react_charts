import React, { Component } from 'react';
import { connect } from 'react-redux';
import io from 'socket.io-client';
import TestChartComponent from '../components/TestChartComponent';

class TestChartContainer extends Component {
  render() {
    return (
      <div>
        <TestChartComponent {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {
    socket: io.connect('http://localhost:8000'),
    margin: {
      top: 20,
      right: 20,
      bottom: 30,
      left: 40,
    },
    duration: 1500,
    limit: 60,
  };
};

TestChartContainer = connect(
  mapStateToProps
)(TestChartContainer);

export default TestChartContainer;
