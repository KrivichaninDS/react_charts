import io from 'socket.io-client';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import GraphComponent from '../components/GraphComponent';
import { getChartData } from '../reducers';


const socket = io.connect('http://localhost:8000');

class GraphContainer extends Component {
  componentDidMount() {
    const { addChartData } = this.props;
    socket.on('connect', () => {
      console.log('Socket connected successfully');
    });

    socket.on('setChartData', (data) => {
      addChartData(data);
    });

    this.fetchData();
  }

  fetchData() {
    const { fetchChartData } = this.props;
    fetchChartData();
  }

  render() {
    return (
      <div>
        <GraphComponent
          {...this.props}
          socket={socket}
        />
      </div>
    );
  }
}

GraphContainer.propTypes = {
  addChartData: PropTypes.func.isRequired,
  fetchChartData: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    graphData: getChartData(state),
    loadChartData: () => {
      socket.emit('getChartData');
    },
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchChartData: () => { dispatch(actions.fetchChartData()); },
    addChartData: (data) => { dispatch(actions.addChartData(data)); },
  };
};

GraphContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GraphContainer);

export default GraphContainer;
