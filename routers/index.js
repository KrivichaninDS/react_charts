const express = require('express');
const router = express.Router();
const path = require('path');

const getChartData = require('./getChartData');

router.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../', 'index.html'));
});

router.use('/chart', getChartData);

module.exports = router;
