import React, { Component, PropTypes } from 'react';
import Axis from './Axis';
import Grid from './Grid';
import * as LineChart from '../d3.Charts/LineChart';

class TestChartComponent extends Component {
  render() {
    const { margin, duration, limit, socket } = this.props;
    let now = new Date(Date.now() - duration);

    const w = 500 - margin.left - margin.right,
      h = 300 - margin.top - margin.bottom,
      transform = `translate(${margin.left},${margin.top})`,
      data = d3.range(limit).map(() => {
        return 0;
      });

    const x = LineChart.x({ now, limit, duration, w }),
      y = LineChart.y({ h }),
      yAxis = LineChart.yAxis({ y }),
      yGrid = LineChart.yGrid({ y, w }),
      line = d3.svg.line()
        .x((d, i) => {
          return x(now - ((limit - 1 - i) * duration));
        })
        .y((d) => {
          return y(d);
        }).interpolate('basis');

    const Emit = () => {
      socket.emit('getData');
    };

    const addData = (chunk) => {
      const path = d3.select('.line');
      data.push(chunk);

      now = new Date();
      path.attr('d', line(data));

      x.domain([now - ((limit - 2) * duration), now - duration]);

      path.attr('transform', null)
        .transition()
        .duration(duration)
        .ease('linear')
        .attr('transform', `translate(${x(now - ((limit - 1) * duration))})`);

      data.shift();
    };

    socket.on('setData', (d) => {
      addData(d.chunk);
    });
    return (<div className="test-chart-body">
      <div>
        <svg className="chart" width={w} height={h}>
          <g transform={transform}>
            <path className="line" d={line(data)} strokeLinecap="round" />
            <Grid h={h} grid={yGrid} gridType="y" />
            <Axis h={h} axis={yAxis} axisType="y" />
          </g>
        </svg>
      </div>
      <button onClick={Emit}>Click</button>
    </div>);
  }
}

TestChartComponent.propTypes = {
  margin: PropTypes.object,
  duration: PropTypes.number,
  limit: PropTypes.number,
  socket: PropTypes.object,
};

export default TestChartComponent;
